module.exports = {
    entry: './assets/js/main.js',
    output: {
        filename: './assets/js/bundle.js'
    },
    module: {
        rules: [{
            test: /\.css$/,
            use: [
                { loader: "style-loader" },
                { loader: "css-loader" }
            ]
        }, {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        }, {
            test: /\.(jpg|png|gif|svg|ico)$/,
            exclude: /(node_modules|bower_components)/,
            use: 'file-loader?name=[name].[ext|html]'
        }]
    }
}
